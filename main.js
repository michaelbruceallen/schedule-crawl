// Modules
const {app} = require('electron')
const mainWindow = require('./src/mainWindow');

// Tools enable electron-reload
require('electron-reload')(__dirname)


// Called when electron is finished initializing
app.on('ready', mainWindow.createWindow)

//Quit app when all windows are closed
app.on('closed', () => {
  app.quit()
});
