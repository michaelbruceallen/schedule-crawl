// Modueles
const fs = require('fs');
const moment = require('moment');
const $ = require('jquery');
const { ipcRenderer } = require('electron');

// Utils
const StorageUtils = require('../app/utils/StorageUtils');


const openDev = (name, msg) => {
  ipcRenderer.send(name, msg);
}

const getStorageDataForBrowserWindow = (siteName) => {
  const data = {};
  // Get request time
  data.requestTime = StorageUtils.getRequestTimeChoice(siteName);
  // Get user query list
  data.queryList = StorageUtils.getShiftQueryListById(siteName);
  // Get start and end date range
  data.dateSpan = getDateSpanFromQueryList(data.queryList);

  const { maxChecked, autoSubmit } = StorageUtils.getRequestOptionDataById(siteName);
  data.maxChecked = maxChecked;
  data.autoSubmit = autoSubmit;

  return data;
}

const getMilliSecondsToLogin = (optionsData) => {
  if (!optionsData.requestTime) optionsData.requestTime = { time: '00:00:00.000', id: '1'}
  const { requestTime } = optionsData;

  if(!requestTime) requestTime.time = '00:00:00.000';
  const now = moment();
  const scheduleRequestTime = moment(requestTime.time, 'HH:mm:ss.SSS');

  return Math.max(0, scheduleRequestTime.diff(now));
}

const getDateSpanFromQueryList = (queryList) => {

  let start, end;

  queryList.map( ({date}) => {

    date = moment(date, 'YYYY-MM-DD')
    if (!start) {
      start = end = moment(date, 'YYYY-MM-DD');
    }
    if (date.diff(start) < 0) start = date;
    if (date.diff(end) > 0) end = date;
  });

  if (queryList.length === 0) {
    const startDate = moment().format('MM/DD/YYYY');
    const endDate = startDate;
    return {
      startDate, endDate
    }
  }

  return {
    startDate: start.format('MM/DD/YYYY'),
    endDate: end.format('MM/DD/YYYY')
  }
}

const filterByOptions = (userQueries, tableDataArray) => {

////// userQueries
// type: "reception"
// date: "2017-05-01"
// endTime: "01:00"
// id: "tvtv4q"
// startTime: "01:00"
// type: "reception"

////// tableDataArray
// dates: "08/29/2016"
// endTime: "07:00pm"
// mealPeriod: "Coffee Break"
// room: "COFFEE BREAK ROLL CALL 7AM"
// select: "boxes1"
// serviceType: "CB"
// startTime: "07:00am"

  let checkBoxMatches = []

  // REQUEST QUERY VALUES
  userQueries.map( (userQuery) => {

    success:
    // TABLE VALUES
    Object.entries(tableDataArray).some( ([key, {
      date, shiftStartTime, shiftEndTime, mealPeriod, select
    }]) => {

      let queryDate = moment(userQuery.date, "YYYY-MM-DD");
      let tableDate = moment(date, "MM/DD/YYYY");

      let queryStartTime = moment(userQuery.startTime, "HH:mm");
      let tableStartTime = moment(shiftStartTime, "HH:mm:aa");

      let queryEndTime = moment(userQuery.endTime, "HH:mm");
      let tableEndTime = moment(shiftEndTime, "HH:mm:aa");

      mealPeriod = lowerAndTrim(mealPeriod);
      type = lowerAndTrim(userQuery.type);

      if ( mealPeriod && mealPeriod.includes(type) || type === '-any-') {
        if (!queryDate.diff(tableDate)) {

          if (!queryStartTime.isValid()
            || queryStartTime.diff(tableStartTime) <= 0 ) {

            if (!queryEndTime.isValid()
              || queryEndTime.diff(tableEndTime) >= 0) {
                checkBoxMatches.push(select);
                return true;
            }
          }
        }
      }
    });
  });

  return checkBoxMatches;

  // To trim and lowerAndTrim potential whitespace and capital letters
  function lowerAndTrim(value) {
    value = (value) ? value.toLowerCase().trim() : value;
    return value;
  }

  return checkboxList;
}



const getTableObject = (tableElement) => {

  const rowTableData = $(tableElement).find('tr')

  // Object for temp storage
  let dataTableObj;

  // Constants for index value
  const SELECT = 0;
  const DATE = 1;
  const MEAL_PERIOD = 2;
  const ROOM = 3;
  const START_TIME = 4;
  const END_TIME = 5;
  const SERVICE_TYPE = 6;

  // Final list of objects to return
  let finalOBjList = []

  function trimText(element) {
    return $.trim(element.innerText)
  }

  rowTableData.each((index, rowData) => {
    // First index is a header. I do not want that.
    if (index > 0) {

      dataTableObj = {}

      $(rowData.querySelectorAll('td')).each((index, colData) => {
        if (!trimText(colData).includes('No shifts are available')) {
          switch (index) {
            case SELECT:
              colData = $(colData).find('input')[0];
              let id = colData.getAttribute('name');
              dataTableObj.select = id;
              break;
            case DATE:
              dataTableObj['date'] = trimText(colData);
              break;
            case MEAL_PERIOD:
              dataTableObj['mealPeriod'] = trimText(colData);
            break;
              case ROOM:
              dataTableObj['room'] = trimText(colData);
            break;
              case START_TIME:
              dataTableObj['shiftStartTime'] = trimText(colData);
            break;
              case END_TIME:
              dataTableObj['shiftEndTime'] = trimText(colData);
            break;
              case SERVICE_TYPE:
              dataTableObj['serviceType'] = trimText(colData);
            break;
          } // Switch end
        }

      }) // ROW data td each loop

      // Push new objects to final list
      finalOBjList.push(dataTableObj);

    } // INDEX > 0
  }) // ROW TABLE forEach

  return finalOBjList;

}

module.exports = {
  openDev,
  getStorageDataForBrowserWindow,
  getMilliSecondsToLogin,
  getDateSpanFromQueryList,
  filterByOptions,
  getTableObject
}
