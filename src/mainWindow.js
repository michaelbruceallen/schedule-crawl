// Modules
const { BrowserWindow, ipcMain } = require('electron');


// BrowserWindow instance (for garbage collection issue)
var win = null;

exports.createWindow = () => {
  // Create Browser window config
  this.win = new BrowserWindow({
    width: 1000,
    minWidth: 400,
    height: 900,
    minHeight: 400
  });

  // Load main window content
  this.win.loadURL(`file://${__dirname}/main.html`);

  // Dev tools
  ipcMain.on('open-dev-tools', () => {
    this.win.toggleDevTools();
  });

  // Listen for window close event
  this.win.on('closed', () => {
    this.win = null;
  });

}
