// Modules
const { ipcRenderer, remote } = require('electron');
const { BrowserWindow, ipcMain } = remote;

const qs = require('querystring');
const fs = require('fs');
const path = require('path');
const moment = require('moment');

// Utils
const RemoteItems = require('./remoteItems.js');
const StorageUtils = require('../app/utils/StorageUtils');

// Login json
const loginData = require('../login.json');
// Options data object

// Instantiate window and vars
let win = null;

let optionsData = {};
let domReady = false;

let loginTimeout, requestTimeout;

ipcMain.on('reload', () => {
  clearInterval(loginTimeout);
  clearInterval(requestTimeout);
})


exports.initBrowser = (siteName, closeWindow, ) => {

  if (!win && !closeWindow) {

    // OPTIONS DATA setup
    optionsData = RemoteItems.getStorageDataForBrowserWindow(siteName);
    optionsData.siteName = siteName;

    // Init new hidden window
    win = new BrowserWindow({
      width: 800,
      height: 1000,
      show: false,
      webPreferences: {
        webSecurity: false
      }
    });

    // Request time and Login 30 seconds before request
    let MS_TO_REQUEST = RemoteItems.getMilliSecondsToLogin(optionsData);
    let MS_TO_LOGIN = MS_TO_REQUEST - 30000;

    console.log(`Login in ${
      MS_TO_LOGIN / 1000 / 60
    } minutes and ${
      MS_TO_LOGIN / 1000
    } seconds.`);

    // LOGIN  and SCHEDULE timers
    loginTimeout = setTimeout( initialLogin, MS_TO_LOGIN );

    win.webContents.once('dom-ready', () => {
      domReady = true;
    });
    //
    requestTimeout = setTimeout( submitRequest, MS_TO_REQUEST );


    // Close window
    win.on('closed', () => {
      ipcRenderer.send('closed-window', 'closed');
      win = null;
      domReady = false;
      console.log("Windows closing...");
    });

  }

  if (closeWindow) {
    ipcRenderer.send('closed-window', 'closed');
    if(win) win.close();
    win = null;
    domReady = false;
  }

}



const submitRequest = () => {

  if (domReady) {
    navigateToSchedule(loginData[optionsData.siteName].url);
  } else {
    win.webContents.once('dom-ready', () => {
      navigateToSchedule(loginData[optionsData.siteName].url);
    });
  }
}

const initialLogin = () => {

  console.log('');
  console.log(moment().format('HH:mm:ss:SS a'));
  console.log('Initial login...');

  let loginDataQuery = qs.stringify({
    Username: `${loginData[optionsData.siteName].Username}`,
    Password: `${loginData[optionsData.siteName].Password}`,
    ClientID: `${loginData[optionsData.siteName].ClientID}`
  });

  const url = `${loginData[optionsData.siteName].url}/login-process.asp`;

  // Login using loginData[siteName] with login-process.asp
  console.log(url);
  win.loadURL(url, {
    postData: [{
      type: 'rawData',
      bytes: Buffer.from(loginDataQuery)
    }],
    extraHeaders: 'Content-Type: application/x-www-form-urlencoded'
  });


}


// NAVIGATE TO SCHEDULE
const navigateToSchedule = (url) => {

  // Load schedule by date
  const scheduleURL = getScheduleURL(url);

  // win.reload();

  win.loadURL(scheduleURL);

  // Dummy webload
  // win.loadURL(`file://${__dirname}/../test/dummy_pages/unsuccesful_shifts.html`);

  // win.loadURL(`file://${__dirname}/dummy_pages/mb no submit.html`);
  // win.loadURL(`file://${__dirname}/dummy_pages/mandalay22.html`);

  win.webContents.openDevTools();

  // Check boxes once dom-ready on Schedule
  win.webContents.once('did-navigate', () => {
    win.webContents.once('dom-ready', () => {

      console.log(`Looking for boxes to check at: ${moment().format('HH:mm:ss:SSS a')}`);
      checkBoxesOnSchedule(optionsData.maxChecked, optionsData.autoSubmit);

    });
  });
}



// CHECKBOXES ON SCHEDULE
const checkBoxesOnSchedule = (maxChecked, isAutoSubmit) => {

  win.show();

  win.webContents.executeJavaScript(`

    console.log("WEBSITE DEV TOOLS!!!");
    const now = new Date();
    console.log(now);
    console.log(now.getMilliseconds());

    const RemoteTools = require(
      ${JSON.stringify(path.join(__dirname, 'remoteItems.js' ))}
    );

    let tableElement = document
      .querySelectorAll('form table.tablewhiteborders')[0]

    if(!tableElement) tableElement = document
      .querySelectorAll('form[name$="form1"] table')[0]

    if (tableElement) {

      const parsedTableHTML = RemoteTools
        .getTableObject(tableElement.outerHTML)

      const checkBoxes = RemoteTools
        .filterByOptions(${JSON.stringify(optionsData.queryList)}, parsedTableHTML)

      // Varible to count checked boxes
      let totalChecked = 0;


      // Some() is used to exit the loop easily.
      checkBoxes.some((selection) => {

        // This will exit the some() loop upon true
        if (totalChecked === ${maxChecked}) return true;

        // Check the selection input checkbox on website
        document.querySelectorAll('input[name=' + selection + ']')[0].checked = true;
        totalChecked++;

      }) // End checkbox

      if (${isAutoSubmit}) {
        console.log("Submitting...");
        submitform('Confirm');
        // window.document.form1.submit();
      }

    } else {
      console.warn("No table data found on page...");
    }

  `);

}

// // CREATE SCHEDULE URL with Querystring
const getScheduleURL = (url) => {

  // Return unescaped URL / characters
  const { startDate } = optionsData.dateSpan;
  const { endDate } = optionsData.dateSpan;

  let queryString = qs.stringify({
    'sstype': 'GroupList',
    'StartDate': startDate,
    'EndDate': endDate,
    'Filter1': '12',
    'Filter2': '15',
    'imageField.x': '11',
    'imageField.y': '18'
  })

  queryString = qs.unescape(queryString)

  return `${url}/schedule_self.asp?${queryString}`
}
