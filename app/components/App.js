// Modules
import React, { Component } from 'react';
import { HashRouter, Switch, Route, Link, browserHistory } from 'react-router-dom';


// Utils
import StorageUtils from '../utils/StorageUtils';

// Components
import OptionsDisplay from  './option_display/OptionDisplay';


class App extends Component {

  render() {
    return (
      <HashRouter>
        <Switch>
          <Route exact path="/" component={this.siteList.bind(this)} />
          <Route path="/:id" component={OptionsDisplay} />
        </Switch>
      </HashRouter>
    );
  };

  siteList() {
    return (
      <main className="container">
        <section className="modal is-active">
          <div className="modal-card">
            <div className="modal-card-body">
              {this.renderSiteList()}
            </div>
          </div>
        </section>
      </main>
    );
  }; // siteList

  renderSiteList() {

    const loginFile = require('../../login.json');
    const loginArray = Object.entries(loginFile);


    return loginArray.map(([name, value]) => {
      StorageUtils.addSiteById(name)
      return (

        <Link to={`/${name}`}
        key={name}
        id={name}
        className="box site"
        >
          <span className="icon">
            <i className="fa fa-book"></i>
          </span>
          {name}
        </Link>
      );
    });
  }; // renderSiteList

}

export default App;
