// Modules
import React, { Component } from 'react';
import moment from 'moment';
import { remote } from 'electron';
const { ipcMain } = remote;


class LoginTime extends Component {

  constructor(props) {
    super(props);
    this.state = {
      h: '',
      m: '',
      s: '',
      p: '',
      soon: false,
    }

  }

  componentDidMount() {

    this.timerID = setInterval( () => this.updateTime(), 1000);

    ipcMain.once('window-closed', () => {
      this.setState({
        countDownOn: false
      })
    });

    ipcMain.once('start-timer', (err, value) => {
      this.setState({
        countDownOn: value
      });
    });

  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  updateTime() {
    var time = this.getTimeArray();

    this.setState({
      h: time.h,
      m: time.m,
      s: time.s,
      p: time.p,
      soon: false
    });
  }

  getTimeArray() {

    const now = moment().format('HH:mm:ss:SSS');

    let [ h, m, s, ms ] = now.split(':');
    let p = 'am';
    if (Number(h) > 12) {
      p = 'pm';
      h = Number(h) - 12;
    }
    return { h, m, s, p };
  }


  render() {

    return (
      <div className="live-time">
        <div className="current-time">
          <span className="h">{this.state.h}</span>:
          <span className="m">{this.state.m}</span>:
          <span className={`s ${this.state.soon ? 'soon' : ''}`}>{this.state.s} </span>

          <span className="p">
            {this.state.p}</span>
        </div>
        <div className="countdown-time">
          <span className={this.state.soon ? 'soon' : ''}>{this.state.countDown}</span>
        </div>


      </div>
    );
  }
}

export default LoginTime;
