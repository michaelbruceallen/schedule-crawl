// Modules
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';

// Actions
import { addRequestTime } from '../../actions';


class AddRequestTime extends Component {

  renderField(field) {

    return (
      <input type={field.type}
             className={field.className}
             step={field.step}
             id={field.id}
             {...field.input}
             />
    )
  }

  onSubmit(values) {
    if (values.requestTime) {
      let { requestTime } = values;
      if (requestTime.length === 5)
      requestTime += ':00.000';
      if (requestTime.length === 8)
      requestTime += '.000';

      this.props.addRequestTime(this.props.currentSiteId, requestTime);
    }
  }

  render() {
    return (
      <div className="add-request-time">
        <form className="add-request-time-form field has-addons"
              onSubmit={this.props.handleSubmit(this.onSubmit.bind(this))}
              >
          <p className="control">
            <Field type="time"
                   step="0.001"
                   id="request_time_input"
                   className="add-request-time input is-small"
                   label="Request Time"
                   name="requestTime"
                   component={this.renderField}
                   />
            </p>
          <p className="control">
            <button className="button is-info is-small" type="action">
              Add Time
            </button>
          </p>
        </form>
      </div>
    )
  }
}



function mapStateToProps(state) {
  return { shiftForm: state.requestForm }
}

// Redux Form connection
AddRequestTime = reduxForm({
  form: 'requestForm'
})(AddRequestTime)

// Redux connect
AddRequestTime = connect(mapStateToProps, {
  addRequestTime
})(AddRequestTime);

export default AddRequestTime;
