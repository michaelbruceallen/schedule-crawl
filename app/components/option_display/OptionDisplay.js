// Modules
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, hashHistory } from 'react-router-dom';
import { openDev } from '../../../src/remoteItems';
import { ipcRenderer } from 'electron';

// Actions
import { getShiftQueryList, getRequestOptionData } from '../../actions';

// Components
import ViewShiftQueries from './ViewShiftQueries';
import AddShiftQuery from './AddShiftQuery';
import AddRequestTime from './AddRequestTime';
import ViewRequestTimes from './ViewRequestTimes';
import StartOptions from './StartOptions';
import CurrentTime from './CurrentTime';


// Login
import loginFile from '../../../login.json';


class QueryOptions extends Component {

  constructor(props) {
    super(props);
    this.siteId = this.props.match.params.id;
    this.name = loginFile[this.siteId].name;
  }

  componentDidMount() {
    this.props.getShiftQueryList(this.siteId);
    this.props.getRequestOptionData(this.siteId);
  }

  handleDevTools() {
    openDev('open-dev-tools', 'open-dev-tools');
  }

  handleMainMenuLink() {
    ipcRenderer.send('reload');
    ipcRenderer.send('closed-window');
    this.props.history.push('/');

  }

  render() {
    return (
        <div className="options-display tile">

            <div className="tile is-vertical">
              <section className="hero is-primary is-bold">
                <div className="hero-body">
                  <div className="container">
                    <h1 className="title">
                      {this.name}
                    </h1>
                    <h2 className="subtitle">
                    </h2>
                  </div>
                </div>
              </section>
              <div className="tile">
                <div className="tile is-child box">
                  <div className="content">
                    <AddShiftQuery currentSiteId={this.siteId} />
                  </div>
                </div>
                <div className="tile is-child box">
                  <div className="content">
                    <ViewShiftQueries currentSiteId={this.siteId} />
                  </div>
                </div>
              </div>
              <div className="tile">
                <div className="tile is-child box">
                  <div className="content">
                    <AddRequestTime currentSiteId={this.siteId} />
                  </div>
                  <div className="content">
                  <ViewRequestTimes currentSiteId={this.siteId} />
                  </div>
                </div>
                <div onClick={this.handleDevTools} className="tile is-child box">
                  <div className="content schedule-status">
                    <CurrentTime currentSiteId={this.siteId} />
                    <div className="countdown-time"></div>
                    <div className="login-status"></div>
                    <div className="main-request-status"></div>
                    <div className="selected-item-status"></div>
                    <div className="success-status"></div>
                  </div>
                </div>
              </div>
              <div className="tile is-child box">
                <StartOptions currentSiteId={this.siteId} />
              </div>
              <div className="tile">
                <a onClick={this.handleMainMenuLink.bind(this)}>
                  Return to Main Menu
                </a>
              </div>
            </div>


        </div>
    )
  }
}


QueryOptions = connect(null, {
  getShiftQueryList,
  getRequestOptionData
})(QueryOptions);

export default QueryOptions;
