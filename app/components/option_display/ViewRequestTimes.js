// Modules
import React, { Component } from 'react';
import { connect } from 'react-redux';

// Actions
import { deleteRequestTime, setRequestTimeChoice } from '../../actions';


class ViewRequestTimes extends Component {

  render() {

    const { handleSubmit } = this.props;

    return (
      <section className="view-request-times">
        <form className="request-time-list">
          {this.renderRequestTimes()}
        </form>
      </section>
    )
  }

  renderRequestTimes() {
    const { choice, times } = this.props.requestOptions;
    if (!times) {
      return (
        <div>You should add some request times...</div>
      )
    }
    return times.map((time) => {
      let isChecked = '';
      if (time.id === choice) {
        isChecked = 'is-active'
      }

      const [hh, h, m ,s, ms, p] = formatTime(time.time);

      return (
        <div key={time.id} className="request-time-section field">
          <p className="control">
            <label className="label">

              <input type="radio"
                     id={time.id}
                     className="request-time-radio"
                     checked={isChecked}
                     name="question"
                     onChange={this.handleSelect.bind(this)}
                     />
              <span className="time-group">
                <span className="time-format h">
                  <span className="time-format hh">{ hh }</span>
                  { h }
                </span>:
                <span className="time-format m">{ m }</span>:
                <span className="time-format s">{ s }</span>.
                <span className="time-format ms">{ ms } </span>
                <span className="time-format p">{ p } </span>
              </span>

              <button id={time.id}
                      onClick={this.handleDelete.bind(this)}
                      className="request-time-delete is-small is-pulled-right delete"></button>
            </label>
          </p>
        </div>
      );
    });

    function formatTime(time) {
      let period = 'AM';
      let [h, m, s] = time.split(':');
      s = s.split('.');
      let hh = '';
      if ( Number(h) > 12 ) {
        period = 'PM';
        h = (Number(h) - 12);
        if (h < 10) hh = 0;
      }
      return [hh, h, m, s[0], s[1], period]
    }
  }
  onsubmit() {
    console.log("Form Submited...");
  }

  handleSelect({ target: { id } }) {
    this.props.setRequestTimeChoice(this.props.currentSiteId, id);
  }

  handleDelete({ target: { id } }) {
    this.props.deleteRequestTime(this.props.currentSiteId, id)
  }

}


// MAP TO PROPS
function mapStateToProps(state) {
  return {
    requestOptions: state.requestOptions
  }
}

// Redux connect
ViewRequestTimes = connect(mapStateToProps, {
  setRequestTimeChoice,
  deleteRequestTime
})(ViewRequestTimes);

export default ViewRequestTimes;
