// Modules
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Field, reduxForm } from 'redux-form';
import { remote, ipcRenderer } from 'electron';
const { ipcMain } = remote;

// Actions
import { changeAutoSubmit, changeMaxChecked } from '../../actions';

// remote site
import remoteSite from '../../../src/remoteSite';

// Utils
import StorageUtils from '../../utils/StorageUtils';


class StartOptions extends Component {

  constructor(props) {
    super(props);

    // NOT SURE HOW TO DO THIS ANY OTHER WAY...
    // WHY IS connect() not adding this to props?
    // What console.log(this) different then (this.props)???
    const options = StorageUtils.getRequestOptionDataById(this.props.currentSiteId);
    this.state = {
      closeWindow: false,
      autoSubmit: options.autoSubmit,
      maxChecked: options.maxChecked
    }
  }


  handleSubmit(values) {

    const choiceTime = StorageUtils.getRequestTimeChoice(this.props.currentSiteId);

    if (choiceTime === '00:00:00.000') return null;

    this.setState({
      closeWindow: (this.state.closeWindow)
        ? false : true
    });

    ipcRenderer.send('start-timer', (this.state.closeWindow)
      ? false : true);
    remoteSite.initBrowser(this.props.currentSiteId, this.state.closeWindow);
  }

  handleMaxChecked({ target: { value }}) {
    console.log("maxChecked: ", value);
    this.setState({
      maxChecked: value
    });
    const { currentSiteId } = this.props;
    this.props.changeMaxChecked(currentSiteId, value);
  }

  handleAutoSubmit({ target: { checked } }) {
    console.log("autoSubmit: ", checked);
    this.setState({
      autoSubmit: checked
    });
    const { currentSiteId } = this.props;
    this.props.changeAutoSubmit(currentSiteId, checked);
  }

  render() {
    return (
      <div className="start-options">
        <p className="control max-checked">
          <label className="label">
            <input type="number"
                   name="maxChecked"
                   className="max-checked-input"
                   value={this.state.maxChecked}
                   onChange={this.handleMaxChecked.bind(this)}
            />
            Max shifts?
          </label>
        </p>
        <p className="control auto-submit">
          <label className="label">
            <input type="checkbox"
                   name="autoSubmit"
                   className="auto-submit-checkbox"
                   onChange={this.handleAutoSubmit.bind(this)}
                   checked={this.state.autoSubmit}
            />
            Auto-submit?
          </label>
        </p>
        <p className="control">
          <button onClick={this.handleSubmit.bind(this)}
                  className={ `start-program-button button
                              ${ (this.state.closeWindow) ?
                                'is-danger is-active' : 'is-primary' }`
          }>{
            (this.state.closeWindow) ? 'Stop Timers' : 'Start Timers'
          }</button>
        </p>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    requestOptions: state.requestOptions
  }
}

// Redux connect
export default connect(mapStateToProps, {
  changeAutoSubmit,
  changeMaxChecked
})(StartOptions);
