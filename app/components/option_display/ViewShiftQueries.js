// Modules
import React, { Component } from 'react';
import { connect } from 'react-redux';

// Actions
import { deleteShiftQuery } from '../../actions';
import { prioritizeShiftQuery } from '../../actions';


class ViewShiftQueries extends Component {

  handlePrioritize(queryId) {
    this.props.prioritizeShiftQuery(this.props.currentSiteId, queryId);
  }

  handleDelete(queryId) {
    this.props.deleteShiftQuery(this.props.currentSiteId, queryId);
  }

  renderShiftQueries() {
    const { shiftQueryList } = this.props;

    if ( shiftQueryList.length === 0 ) {
      return (
        <tr className="no-data">
          <td>You should save some data...</td>
        </tr>
      )
    }

    return shiftQueryList.map((shiftQuery) => {

      let {id, type, date, startTime, endTime} = shiftQuery;

      const timeFormat = (time) => {
        if (!time || time === '-Any-') return '-Any-';

        let  [ h, m ] = time.split(':');
        let period = 'a';
        if ( Number(h) > 12 ) {
          period = 'p';
          h = Number(h) - 12;
        }
        return `${h}:${m}${period}`;
      }

      startTime = timeFormat(startTime);
      endTime = timeFormat(endTime);

      return (
          <tr key={id}>
            <td>
              <a className="icon is-small is-left">
                <i onClick={() => this.handlePrioritize(id)} className="fa fa-arrow-up"></i>
              </a>
            </td>

            <td className="type">{type}</td>
            <td className="date"><abbr title={date}>{ date.split('-')[1] +'.'+ date.split('-')[2] }</abbr></td>
            <td className="start-time">{startTime}</td>
            <td className="end-time">{endTime}</td>
            <td>
              <a className="icon is-small is-right">
                <i onClick={() => this.handleDelete(id)} className="fa fa-trash"></i>
              </a>
            </td>
          </tr>

      )
    })
  }

  render() {
    return (
      <aside className="view-shift-queries">
        <table className="table query-table">
          <thead>
            <tr>
              <th><abbr title="Priority">Pr.</abbr></th>
              <th><abbr title="Shift Type">Type</abbr></th>
              <th>Date</th>
              <th><abbr title="Start Time">Start</abbr></th>
              <th><abbr title="End Time">End</abbr></th>
              <th>X</th>
            </tr>
          </thead>
          <tbody>
            {this.renderShiftQueries()}
          </tbody>
        </table>
      </aside>
    )
  }
}

function mapStateToProps(state) {
  return { shiftQueryList: state.shiftQueryList }
}

// Redux connect
ViewShiftQueries = connect(
  mapStateToProps, {
    deleteShiftQuery,
    prioritizeShiftQuery,
  }
)(ViewShiftQueries);

export default ViewShiftQueries;
