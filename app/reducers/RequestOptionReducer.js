// Types
import { ADD_REQUEST_TIME } from '../actions/types';
import { DELETE_REQUEST_TIME } from '../actions/types';
import { GET_REQUEST_TIME_DATA } from '../actions/types';
import { SET_REQUEST_TIME_CHOICE } from '../actions/types';
import { GET_REQUEST_OPTION_DATA } from '../actions/types';


export default function (state = {}, action) {


  switch (action.type) {

    case ADD_REQUEST_TIME:
    case DELETE_REQUEST_TIME:
    case GET_REQUEST_OPTION_DATA:
    case SET_REQUEST_TIME_CHOICE:
      if (action.payload)
        return action.payload;

    default:
      return state;
  }

}
