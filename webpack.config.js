// Modules
const path = require('path');
const webpack = require('webpack');


module.exports = {

  // Entry index.js
  entry: './app/index.js',

  // Output build/bundle.js
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      }
    ]
  },
  target: 'electron-renderer',
  node: {
    __dirname: false
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery'
    })
  ]
}
