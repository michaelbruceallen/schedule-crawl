// Helpers
import { expect } from '../test_helper';

// Reducers and Types
import ShiftQueryReducer from '../../app/reducers/ShiftQueryReducer';
import { GET_SHIFT_QUERY_LIST } from '../../app/actions/types';


describe("Shift Query Reducer", () => {

  it("handles unknown type", () => {
    expect(ShiftQueryReducer(undefined, {})).to.eql([]);
  })

  it("handles action of GET_SHIFT_QUERY_LIST", () => {
    const action = {
      payload: ['shift query'],
      type: GET_SHIFT_QUERY_LIST
    };

    expect(ShiftQueryReducer([], action)).to.eql(['shift query']);
  });

})
