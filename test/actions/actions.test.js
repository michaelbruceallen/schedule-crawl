// Helpers
import { expect } from '../test_helper';

// Actions
import { addNewList } from '../../app/actions';
import { addShiftQuery } from '../../app/actions';
import { getShiftQueryList } from '../../app/actions';
import { setRequestTimeChoice } from '../../app/actions';
import { getRequestTimeData } from '../../app/actions';
import { getRequestOptionData } from '../../app/actions';
import { addRequestTime } from '../../app/actions';
import { prioritizeShiftQuery } from '../../app/actions';
import { deleteShiftQuery } from '../../app/actions';
import { deleteRequestTime } from '../../app/actions';
import { changeAutoSubmit } from '../../app/actions';
import { changeMaxChecked } from '../../app/actions';

// Types
import { ADD_NEW_LIST } from '../../app/actions/types';
import { ADD_SHIFT_QUERY } from '../../app/actions/types';
import { DELETE_SHIFT_QUERY } from '../../app/actions/types';
import { GET_SHIFT_QUERY_LIST } from '../../app/actions/types';
import { ADD_REQUEST_TIME } from '../../app/actions/types';
import { DELETE_REQUEST_TIME } from '../../app/actions/types';
import { SET_REQUEST_TIME_CHOICE } from '../../app/actions/types';
import { GET_REQUEST_TIME_DATA } from '../../app/actions/types';
import { GET_REQUEST_OPTION_DATA } from '../../app/actions/types';
import { PRIORITIZE_SHIFT_QUERY } from '../../app/actions/types';
import { CHANGE_AUTO_SUBMIT } from '../../app/actions/types';
import { CHANGE_MAX_CHECKED } from '../../app/actions/types';

// Utils
import * as Utils from '../../app/utils/StorageUtils';


describe("Actions", () => {

  let action;
  const SITE_ID = 'mandalay';

  describe("CHANGE_MAX_CHECKED", () => {

    beforeEach(() => {
      Utils.addSiteById(SITE_ID);
      action = changeMaxChecked(SITE_ID, 3);
    });

    it("has correct type", () => {
      expect(action.type).to.equal(CHANGE_MAX_CHECKED);
    })

    it("has correct payload", () => {
      expect(action.payload).to.have.keys(['times', 'choice', 'maxChecked', 'autoSubmit']);
    });

    it("has correct value", () => {
      expect(action.payload.maxChecked).to.equal(3);
    });

  });

  describe("CHANGE_AUTO_SUBMIT", () => {

    beforeEach(() => {
      action = changeAutoSubmit(SITE_ID, true);
    });

    it("has correct type", () => {
      expect(action.type).to.equal(CHANGE_AUTO_SUBMIT);
    })

    it("has correct payload", () => {
      expect(action.payload).to.have.keys(['times', 'choice', 'maxChecked', 'autoSubmit']);
    });

  });

  describe("SET_REQUEST_TIME_CHOICE", () => {

    beforeEach(() => {
      action = setRequestTimeChoice(SITE_ID);
    });

    it("has correct type", () => {
      expect(action.type).to.equal(SET_REQUEST_TIME_CHOICE);
    })

    it("has correct payload", () => {
      expect(action.payload).to.have.keys(['times', 'choice', 'maxChecked', 'autoSubmit']);
    });

  });

  describe("DELETE_REQUEST_TIME", () => {

    beforeEach(() => {
      action = deleteRequestTime(SITE_ID);
    });

    it("has correct type", () => {
      expect(action.type).to.equal(DELETE_REQUEST_TIME);
    })

    it("has correct payload", () => {
      expect(action.payload).to.have.keys(['times', 'choice', 'maxChecked', 'autoSubmit']);
    });
  })

  describe("DELETE_SHIFT_QUERY", () => {

    beforeEach(() => {
      action = deleteShiftQuery(SITE_ID);
    });

    it("has correct type", () => {
      expect(action.type).to.equal(DELETE_SHIFT_QUERY);
    });

    it("has correct payload", () => {
      expect(action.payload).to.be.instanceof(Array);
    })

  });

  describe("PRIORITIZE_SHIFT_QUERY", () => {

    beforeEach(() => {
      Utils.addSiteById(SITE_ID);
      action = prioritizeShiftQuery(SITE_ID, undefined);
    });

    afterEach(() => {
      Utils.deleteSiteById(SITE_ID);
    });

    it("has correct type", () => {
      expect(action.type).to.equal(PRIORITIZE_SHIFT_QUERY);
    });

    it("has correct payload", () => {
      expect(action.payload).to.be.instanceof(Array);
    })

  });

  describe("GET_SHIFT_QUERY_LIST", () => {

    it("has correct type", () => {
      const action = getShiftQueryList(SITE_ID);
      expect(action.type).to.equal(GET_SHIFT_QUERY_LIST)
    });

    it("has correct payload", () => {
      const action = getShiftQueryList(SITE_ID);
      expect(action.payload).to.be.instanceof(Array);
    });
  });

  // TODO: How do I handle this type of action!!!
  describe("ADD_SHIFT_QUERY", () => {

    beforeEach(() => {
      Utils.addSiteById(SITE_ID);
      action = addShiftQuery(SITE_ID, { type: 'lunch'});
    });

    afterEach(() => {
      Utils.deleteSiteById(SITE_ID)
    });

    it("has correct type", () => {
      expect(action.type).to.equal(ADD_SHIFT_QUERY);
    });

    it("adds generateUID", () => {
      const { shiftQueries } = Utils.getSiteById(SITE_ID);
      expect(shiftQueries.length).to.equal(1);
      expect(shiftQueries[0].id).to.exist;
    });

    it("has type: 'lunch'", () => {
      const { shiftQueries } = Utils.getSiteById(SITE_ID);
      expect(shiftQueries.length).to.equal(1);
      expect(shiftQueries[0].type).to.exist;
      expect(shiftQueries[0].type).to.equal('lunch')
    });

    it("has correct payload", () => {
      expect(action.payload).to.be.instanceof(Array);
      expect(action.payload[0]).to.have.keys(['type', 'id'])
    });

  });

  describe("GET_REQUEST_OPTION_DATA", () => {

    it("has correct type", () => {
      const action = getRequestOptionData(SITE_ID);
      expect(action.type).to.equal(GET_REQUEST_OPTION_DATA);
    });

    it("has correct payload", () => {
      const action = getRequestOptionData(SITE_ID);
      expect(action.payload.times).to.be.instanceof(Array);
      expect(action.payload.choice).to.equal(0);
    });

  });

  describe("ADD_REQUEST_TIME", () => {

    before(() => {
      action = addRequestTime(SITE_ID, '2:50:30.000');
    });

    after(() => {
      Utils.deleteSiteById(SITE_ID);
    });

    it("has correct type", () => {
      expect(action.type).to.equal(ADD_REQUEST_TIME)
    });

    it("has a generatedUID", () => {
      const requestOptionData = Utils.getRequestOptionDataById(SITE_ID);
      expect(requestOptionData.times[0]).to.have.keys(['id', 'time'])
    });

    it("adds time data", () => {
      const requestOptionData = Utils.getRequestOptionDataById(SITE_ID);
      expect(requestOptionData.times.length).to.equal(1);
      expect(requestOptionData.times[0].time).to.equal('2:50:30.000');
    });

    it("has correct payload", () => {
      expect(action.payload).to.have.keys(['times', 'choice', 'maxChecked', 'autoSubmit'])
    })

  });

});
