// Helpers
import { expect, renderComponent } from '../../test_helper';

// Components
import StartOptions from '../../../app/components/option_display/StartOptions'


describe("Start Options", () => {

  let component;

  beforeEach(() => {
    const props = {
      requestOptions: {
        times: [],
        choice: '',
        maxChecked: 1,
        autoSubmit: true
      }
    }
    component = renderComponent(StartOptions, props);
  });

  it("exists", () => {
    expect(component).to.exist;
  });

  it("has .start-program-button", () => {
    expect(component.find('.start-program-button')).to.exist;
  });

  describe("Max Checked", () => {

    let maxCheckedInput;

    beforeEach(() => {
      maxCheckedInput = component.find('.max-checked-input')
    });

    it("has .max-checked-input", () => {
      expect(maxCheckedInput).to.exist;
    });

    it("has props value of 1", () => {
      expect(maxCheckedInput.val()).to.equal('1');
    });

  })

  describe("Auto Submit", () => {

    let autoSubmitCheckbox;

    beforeEach(() => {
      autoSubmitCheckbox = component.find('.auto-submit-checkbox');
    });

    it("has .auto-submit-checkbox", () => {
      expect(autoSubmitCheckbox).to.exist;
    });

    it("has props value of true", () => {
      expect(autoSubmitCheckbox).to.be.checked;
    });

    // TODO: WHY!?
    xit("changes on click", () => {
      autoSubmitCheckbox.simulate('change');
      expect(autoSubmitCheckbox).to.not.be.checked;
    });

  });

});
