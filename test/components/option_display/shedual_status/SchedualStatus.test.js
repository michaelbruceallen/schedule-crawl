// Helpers
import { expect, renderComponent } from '../../../test_helper';

// Components
import SchedualStatus from '../../../../app/components/option_display/schedual_status/SchedualStatus';


describe("SchedualStatus", () => {

  let component;

  beforeEach(() => {
    component = renderComponent(SchedualStatus);
  })

  it("exists", () => {
    expect(component).to.exist;;
  });

  it("has login-time", () => {
    expect(component.find('.login-time')).to.exist;
  });

  it("has current-time", () => {
    expect(component.find('.current-time')).to.exist;
  });

  it("has countdown-time", () => {
    expect(component.find('.countdown-time')).to.exist;
  });

  it("has login-status", () => {
    expect(component.find('.login-status')).to.exist;
  });

  it("has main-request-status", () => {
    expect(component.find('.main-request-status')).to.exist;
  });

  it("has selected-item-status", () => {
    expect(component.find('.selected-item-status')).to.exist;
  });

  it("has success-status", () => {
    expect(component.find('.success-status')).to.exist;
  });

});
