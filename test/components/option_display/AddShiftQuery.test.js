// Helpers
import { expect, renderComponent } from '../../test_helper';

// Components
import AddShiftQuery from '../../../app/components/option_display/AddShiftQuery';


describe("AddShiftQuery", () => {

  describe("shift inputs", () => {

    let component = renderComponent(AddShiftQuery);
    let shiftSubmit = component.find('button.shift-submit');
    let shiftType, shiftDate, startTime, endTime;

    describe("shift-type input", () => {

      beforeEach(() => {
        shiftType = renderComponent(AddShiftQuery)
      });

      it("has .shift-type input", () => {
        expect(shiftType).to.exist;
      });

      it("displays text as typed", () => {
        shiftType.simulate('change', 'lunch');
        expect(shiftType).to.have.value('lunch');
      });

    });

    describe("shift-date input", () => {

      beforeEach(() => {
        shiftDate = renderComponent(AddShiftQuery)
          .find('.shift-date input');
      });

      it("has .shift-date input", () => {
        expect(shiftDate).to.exist;
      });

      it("displays text as typed", () => {
        shiftDate.simulate('change', '05/26/2017');
        expect(shiftDate).to.have.value('05/26/2017');
      });

    });

    describe("shift-time inputs", () => {

      describe("startTime", () => {

        startTime = renderComponent(AddShiftQuery)
          .find('.shift-time input[name=startTime]');

        it("has .shift-time input", () => {
          expect(startTime).to.exist;
        });

        it("displays text as typed", () => {
          startTime.simulate('change', '05/26/2017');
          expect(startTime).to.have.value('05/26/2017');
        });

      });

      describe("endTime", () => {

        endTime = renderComponent(AddShiftQuery)
          .find('.shift-time input[name=endTime]');

        it("has end .shift-time input", () => {
          expect(endTime).to.exist;
        });

        it("displays text as typed", () => {
          endTime.simulate('change', '05/26/2017');
          expect(endTime).to.have.value('05/26/2017');
        });

      });

    });

    describe("validation", () => {

      // !!! .parent().parent() to find a .error className on outer field

      describe("error", () => {

        it("shift-type has error", () => {
          shiftType = component.find('.shift-type input');
          shiftType.simulate('focus');
          shiftType.simulate('blur');
          expect(shiftType.parent().parent().find('.error')).to.exist;
        });

        it("shift-date has error", () => {
          shiftDate = component.find('.shift-date input');
          shiftDate.simulate('change', '02/10');
          shiftDate.simulate('blur');
          expect(shiftDate.parent().parent().find('.error')).to.exist;
        });

        it("startTime has error", () => {
          startTime = component.find('.shift-time input[name=startTime]');
          startTime.simulate('change', '02');
          startTime.simulate('blur');
          expect(startTime.parent().parent().find('.error')).to.exist;
        });

        it("endTime has error", () => {
          endTime = component.find('.shift-time input[name=endTime]');
          endTime.simulate('change', '02');
          endTime.simulate('blur');
          expect(endTime.parent().parent().find('.error')).to.exist;
        });

      });

      describe("No errors", () => {

        it("shift-type no error", () => {
          shiftType = component.find('.shift-type input');
          shiftType.simulate('change', 'dinner');
          expect(shiftType.parent().parent().find('.error')).to.not.exist;
        });

        it("shift-date no error", () => {
          shiftDate = component.find('.shift-date input');
          shiftDate.simulate('change', '2017-02-10');
          expect(shiftDate.parent().parent().find('.error')).to.not.exist;
        });

        it("startTime no error", () => {
          shiftDate = component.find('.shift-time input[name=startTime]');
          shiftDate.simulate('change', '02:30');
          expect(shiftDate.parent().parent().find('.error')).to.not.exist;
        });

        it("endTime no error", () => {
          shiftDate = component.find('.shift-time input[name=endTime]');
          shiftDate.simulate('change', '14:30');
          expect(shiftDate.parent().parent().find('.error')).to.not.exist;
        });

      });
    });


  });
});
