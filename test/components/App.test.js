// Modules
import React from 'react';

// Helpers
import { expect, renderComponent } from '../test_helper';

// Components
import MainMenu from '../../app/components/App';



describe("MainMenu", () => {

  let component;

  beforeEach(() => {
    component = renderComponent(MainMenu);
  })

  it("should exist", () => {
    expect(component).to.exist;
  })

  it("shows a OptionDisplay on route /:id", () => {
    // TODO: How to do this?
  });

});
