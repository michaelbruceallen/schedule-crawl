// Helpers
import { expect } from '../test_helper';

// File to test
import * as Utils from '../../app/utils/StorageUtils';


describe("StorageUtils", () => {

  let appData;
  const SITE_ID = 'test_site_id';

  beforeEach(() => {
    Utils.addSiteById(SITE_ID);
  });

  afterEach(() => {
    Utils.deleteSiteById(SITE_ID);
  });

  describe("Sites", () => {

    it("creates site", () => {
      appData = Utils.getAppData();
      expect(appData[SITE_ID]).to.exist;
      expect(appData[SITE_ID].shiftQueries).to.exist;
      expect(appData[SITE_ID].requestOptions).to.exist;
      expect(appData[SITE_ID].requestOptions.times).to.exist;
      expect(appData[SITE_ID].requestOptions.choice).to.exist;
    });

    it("deletes site", () => {
      const tempSite = 'delete_this';

      Utils.addSiteById(tempSite)
      appData = Utils.getAppData();
      expect(appData[tempSite]).to.exist;

      Utils.deleteSiteById(tempSite)
      appData = Utils.getAppData();
      expect(appData[tempSite]).to.not.exist;
    });

  });

  describe("Request Options", () => {

  it("can change autoSubmit", () => {
    Utils.changeAutoSubmitById(SITE_ID, false);
    const requestOptions = Utils.getRequestOptionDataById(SITE_ID);
    expect(requestOptions.autoSubmit).to.be.false;
  });

  it("can change maxChecked", () => {
    Utils.changeMaxCheckedById(SITE_ID, 4);
    const requestOptions = Utils.getRequestOptionDataById(SITE_ID);
    expect(requestOptions.maxChecked).to.be.equal(4);
  });

  });

  describe("Request Times", () => {

    beforeEach(() => {
      const requestTime = '14:30:00.000';
      Utils.addRequestTimeById(SITE_ID, requestTime);
    })

    it("creates", () => {
      // GET SITE DATA
      const { requestOptions } = Utils.getSiteById(SITE_ID);
      expect(requestOptions.times[0].time).to.equal('14:30:00.000');
      expect(requestOptions.times[0].id).to.exist;
    });

    it("loads options", () => {
      // GET REQUEST TIME DATA
      const optionData = Utils.getRequestOptionDataById(SITE_ID);

      expect(optionData).to.have.keys(['times', 'choice', 'maxChecked', 'autoSubmit'])
      const { times, choice } = optionData;
      expect(choice).to.equal(0);
      expect(times[0].id).to.exist;
      expect(times[0].time).to.equal('14:30:00.000');
    });

    it("loads times", () => {
      // GET REQUEST TIME DATA
      const timeList = Utils.getRequestTimesById(SITE_ID);

      expect(timeList[0]).to.have.keys(['time', 'id']);
      expect(timeList).to.be.instanceOf(Array);
      expect(timeList[0].id).to.exist
      expect(timeList[0].time).to.equal('14:30:00.000');
    });

    it("deletes", () => {
      let optionData = Utils.getRequestOptionDataById(SITE_ID);
      const id = optionData.times[0].id;
      // DELETE TIME BY ID
      Utils.deleteRequestTimeById(SITE_ID, id);
      optionData = Utils.getRequestOptionDataById(SITE_ID);
      expect(optionData.times.length).to.equal(0);
    });

    it("selects request times", () => {
      Utils.setRequestTimeChoiceById(SITE_ID, "abc");
      let optionData = Utils.getRequestOptionDataById(SITE_ID);
      expect(optionData.choice).to.equal("abc");

    })

  });

  describe("Shift Queries", () => {

    let appData;

    beforeEach(() => {
      const shiftQuery_1 = {
        type: 'lunch',
        date: '2017-04-01',
        startTime: '01:30',
        endTime: '13:00'
      }
      const shiftQuery_2 = {
        type: 'reception',
        date: '2017-12-03',
        startTime: '11:00',
        endTime: '02:45'
      }
      Utils.addShiftQueryById(SITE_ID, shiftQuery_1);
      Utils.addShiftQueryById(SITE_ID, shiftQuery_2);
    });

    it("creates new", () => {
      const { shiftQueries } = Utils.getSiteById(SITE_ID);
      expect(shiftQueries[0]).to.contain.all.keys([
        'id',
        'type',
        'date',
        'startTime',
        'endTime'
      ]);
    });

    describe("by ID", () => {

      let queryList;

      beforeEach(() => {
        queryList = Utils.getShiftQueryListById(SITE_ID);
      });

      it("gets list", () => {
        expect(queryList.length).to.equal(2);
      });


      it("deletes", () => {
        const idToDelete = queryList[0].id;
        Utils.deleteShiftQueryById(SITE_ID, idToDelete);
        // Load updated queries
        queryList = Utils.getShiftQueryListById(SITE_ID);
        expect(queryList).to.have.lengthOf(1);
      });

      it("shifts position", () => {
        const priorityId = queryList[1].id;
        Utils.prioritizeShiftQueryById(SITE_ID, priorityId);
        // Load updated queries
        queryList = Utils.getShiftQueryListById(SITE_ID);
        expect(queryList[0].id).to.equal(priorityId);
      });

    });

  });

});
