# Login Screen
* Simply select a site
* it takes you to that sites option screen

# Option Screen
* you can pick a set of custom dats and times.
* listed you see Monday, Wednesday And Friday.
* You can store your user and password
* Then simple click crawl
  * if there is a captch it will watch for you to do the captia
  * then it continues magic


// Embarrassed may 19th.
Wow! I just realize if a parent component calls redux conenct function, then a child component can mapStateToProps.

  This is the plan :)

![](./journal/login-idea.jpg)


How to access DOM with webContent
http://stackoverflow.com/questions/38962385/how-can-i-access-dom-element-via-electron
http://stackoverflow.com/questions/35019166/how-to-get-dom-tree-from-browserwindow-in-electron-app

Save HTML
https://discuss.atom.io/t/accessing-the-dom/22152/7

How can I make a POST request and use that

### Site Options

![](./journal/20170512_093222.jpg)


> Questions
* Where should I dispatch props? SiteOptions or QueryOptions?
* How do I unit test save to JSON file?


###### Add
* Type input
* Time (range) input
* Date input
* Add button

###### List
* View (type - date - startTime - endTime)


* move up
  * click to move up in list view
  * if at top, do nothing


* delete
  * deletes query


###### Actions
* ADD_OPTION_QUERY
  * { queryId, type, startTime, endTime, date }


* GET_OPTION_QUERY_LIST,
  * [] of query options


* SHIFT_QUERY_UP
  * { queryId }


* DELETE_QUERY_OPTION
  * { siteId, queryId }



### Login Time

###### Add
* Time input
* Add button

###### List times
* Radio button
* Time
* Delete button

###### Actions
* ADD_REQUEST_TIME
  * { timeString }


* REMOVE_REQUEST_TIME
  * { timeId }


* SELECT_REQUEST_TIME
  * { timeID }



## Status View

###### Time view
* Login time
* Current time

###### Log View
* Initial login...
* main request...
* Successful shifts...
  * [] of selected shifts
  * time of submit
* Success / Fail message


## Start
###### Button
* Starts app
