userOptions = {
  mealType: ['lunch'],
  date: ['08/26/2016', '08/27/2016', '08/29/2016']
}

scheduleData = [
  {
    select: "boxes1",
    date: "08/26/2016",
    mealPeriod: "Lunch",
  },
  {
    select: "boxes2",
    date: "08/27/2016",
    mealPeriod: "Lunch"
  },
  {
    select: "boxes3",
    date: "08/28/2016",
    mealPeriod: "Lunch"
  },
  {
    select: "boxes4",
    date: "08/29/2016",
    mealPeriod: "Lunch"
  }
]

// Lunch: boxes1, boxes3, boxes4
// Date : boxes1, boxes2, boxes4
// Only boxes 1 & 4 should be returned as ['boxes1', 'boxes4'].

let checkboxesToCheck = []

// Iterate scheduleData Array
Object.entries(scheduleData).forEach(([i, sheduleObject]) => {

  // Iterate USER option key & values

  let optionsThatMatch = 0;
  Object.entries(userOptions)
    .forEach(([userKey, userValues], keyIndex, currentOptionSet) => {
      // Iterate schedule OBJECT key & values
      Object.entries(sheduleObject)
        .forEach(([scheduleIndex, scheduleValue])  => {

          // Check list of USER options
          userValues.forEach((userValue) => {

            // Check and clean remove whtiespace and lowercase
            if (cleanAndCompare(scheduleValue, userValue)) {
              // currentOptionSet must
              optionsThatMatch++;
            }
          })
        })

        // All current option sets must be true to push box
        if(optionsThatMatch === currentOptionSet.length) {
          checkboxesToCheck.push(sheduleObject.select);
        }



    })


})

console.log(checkboxesToCheck);

// To trim and clean potential whitespace and capital letters
function cleanAndCompare(value1, value2) {
  value1 = value1.toLowerCase().trim();
  value2 = value2.toLowerCase().trim();
  return value1 === value2;
}
