const { app, BrowserWindow } = require('electron');

let win;

function createWindow() {

    win = new BrowserWindow({ width: 1400, height: 1000 })
    win.openDevTools();

    // First URL
    win.loadURL('https://www.google.com')

    // Once dom-ready
    win.webContents.once('dom-ready', () => {

        // THIS WORKS!!!
        win.webContents.executeJavaScript(`
      console.log("This loads no problem!");
    `)

        // Second URL
        win.loadURL('https://github.com/electron/electron');

        // Once did-navigate seems to function fine
        win.webContents.once('did-navigate', () => {

            // THIS WORKS!!! So did-navigate is working!
            console.log("Main view logs this no problem....");

            win.webContents.once('dom-ready', () => {
              // NOT WORKING!!! Why?
              win.webContents.executeJavaScript(`
                
                console.log("I canot see this nor the affects of the code below...");

                const form = document.querySelectorAll('form.js-site-search-form')[0];

                const input = form.querySelectorAll('input.header-search-input')[0]

                input.value = 'docs';

                form.submit();

              `)

            })
        });
    })
    }

    app.on('ready', createWindow);

    app.on('window-all-closed', () => {
        if (process.platform !== 'darwin') {
            app.quit();
        }
    });
