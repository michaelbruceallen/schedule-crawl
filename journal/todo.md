* Add a timer so the self-schedule only loads at that moment.
* Add date options.
* Check possibility of multiple times.
  * Can these be opened in other windows?
  * Does it need to be a child window?
